import java.io.*;
import java.net.*;
import java.util.concurrent.Semaphore;
import com.sun.nio.sctp.*;

import java.util.ArrayList;
import java.nio.*;


public class Project1 {
	public static final int MESSAGE_SIZE = 100;
	public static final String CONFIG_FILE = "config.txt";
	
	public static final int MESSAGE_TYPE_BUILD_TREE = 0;
	public static final int MESSAGE_TYPE_BROADCAST = 1;
	public static final int MESSAGE_TYPE_ACK = 2;
	public static final int MESSAGE_TYPE_NACK = 3;
	public static final int MESSAGE_TYPE_BROADCAST_RESPONSE = 4;
	public static final String HOST_SUFFIX = ".utdallas.edu";
	
	static Semaphore sem = new Semaphore(1);
	static int hostId;
	Thread Trd_Server = null;
	static boolean first_build_mes = true;
	//static int tree_resp_count = 0;
	//static int bc_resp_count = 0;
	//static final int def_sctp_port = 6001;
	static boolean tree_complete = false;
	static int bc_count = 0;//the number of broadcasts which have been completed
	static int bc_order = -1; //The order to send broadcast
	int hostNum = -1;
	//String hostName = null;
	class node{
		String hostName;
		int port;
	};
	static int TreeNeighbors[] = new int[10];
	static int TreeNeighborsNo = 0;
	static int FileNeighbors[] = new int[10];
	static int FileNeighborsNo = 0;
	
	static ArrayList<node> allNodes = new ArrayList<node>();
	
	int readConfigFile(int hostId)
	{
		int i;
		int count_nodes = 0;
		hostNum = 0;
		boolean gettingNo = false;
		boolean gettingOrder = false;
		int tmp_bc_counter = 0;
		System.out.println("Node "+ hostId + ": Starting to read config file!");
		try{
			FileInputStream Fin = new FileInputStream(CONFIG_FILE);
			BufferedReader Reader = new BufferedReader(new InputStreamReader(Fin));
			String content = Reader.readLine();
			while(content != null)
			{
				if(gettingNo == true)
				{
					String[] line = content.split("\\s+");
					hostNum = Integer.valueOf(line[0]);
					gettingNo = false;
					System.out.println("Node "+ hostId + ": There are " + hostNum + " hosts");
					content = Reader.readLine();
					continue;
				}
				else if(gettingOrder == true)
				{
					tmp_bc_counter ++;
					String[] line = content.split("\\s+");
					int n = Integer.valueOf(line[0]);
					if(n == hostId)
					{
						bc_order = tmp_bc_counter;
					}
					content = Reader.readLine();
					continue;
				}
				if(content.startsWith("dc"))
				{
					count_nodes ++;
					int end = content.indexOf('#');
					String subContent;
					if(end != -1)
					{
						subContent = content.substring(0, end-1);
					}
					else
					{
						subContent = content;
					}
					String[] line = subContent.split("\\s+");
					
					i = 0;
					while(i < line.length)
					{
						if(i == 0)
						{
							node n = new node();
							n.hostName = line[0];
							n.port = Integer.valueOf(line[1]);
							allNodes.add(n);
							
							System.out.println("Node " + hostId+ ": node " + count_nodes + " is " + allNodes.get(count_nodes -1).hostName + " port is " + allNodes.get(count_nodes -1).port);
							if(hostId == count_nodes)//??
							{
								i = i+2;
								continue;
							}
							else
								break;
						}
						else
						{
							FileNeighbors[FileNeighborsNo] = Integer.valueOf(line[i]);
							FileNeighborsNo++;
						}
						i++;
					}
					
						
				}
				else if(content.startsWith("# Number of nodes"))
				{
					gettingNo = true;
				}
				else if(content.startsWith("# Broadcast nodes list"))//broadcast nodes list to get bc_order
				{
					gettingOrder = true;
				}
				content = Reader.readLine();
						
			}
			//System.out.println("");
			System.out.print("Node "+ hostId+ ": The neighbors read from file are/is: ");
			for(int j = 0; j<FileNeighborsNo; j++)
			{
				System.out.print(" " + FileNeighbors[j]);
			}
			System.out.println("");
			//System.out.println("");
			Reader.close();
		}catch(FileNotFoundException e)
		{
			return 1;// fail
		}catch(IOException e)
		{
			return 1;
		}
		if(gettingNo == true)//port is not got
		{
			return 1;
		}
		else
			return 0;
	}
	int checkPort(String s)
	{
		return 0;
	}
	int checkNo(String s)
	{
		return 0;
	}
	void runServer()
	{
		SctpServer ss = new SctpServer();
		Trd_Server = new Thread(ss);
		Trd_Server.start();
	}
	Thread getSctpServer()
	{
		return Trd_Server;
	}
	class BroadCast_Service implements Runnable{
		int messageType;
		SctpChannel sc;
		int hostFrom;
		BroadCast_Service(int mesType, SctpChannel schannel, int host_from)
		{
			messageType = mesType;
			sc = schannel;
			hostFrom = host_from;
		}
		
		public void run()
		{
			switch(messageType)
			{
			case 0:   //build tree
				buildSpanningTree(sc);
				break;
			case 1:   //broad cast
				Broadcast(sc);
				break;
			default:
				break;
			}
		}
		void buildSpanningTree(SctpChannel schannel)
		{
			Thread[] t_tree = new Thread[10];
			
			//tree_resp_count = 0;
			System.out.println("Node "+ hostId + ": Spanning tree begins to build! ");
			if(FileNeighborsNo == 1 && schannel != null)
			{
				sendResponse(MESSAGE_TYPE_ACK, schannel);
				printMessage(MESSAGE_TYPE_ACK, hostId, hostFrom, true);
				//System.out.println("");
				System.out.print("Node " + hostId + " has " + TreeNeighborsNo + " tree Neighbors. They are/It is: ");
				for(int i = 0; i< TreeNeighborsNo; i++)
				{
					System.out.print(" " + TreeNeighbors[i]);
				}
				//System.out.println("");
				System.out.println("");
				try
				{
					schannel.close();
				}catch(IOException e)
				{
					
				}
				return;
			}
			for(int i = 0; i<FileNeighborsNo; i++)
			{
				SctpClient sc = new SctpClient(FileNeighbors[i], MESSAGE_TYPE_BUILD_TREE);
				t_tree[i] = new Thread(sc);
				t_tree[i].start();
			}
			try{
				for(int i = 0; i<FileNeighborsNo; i++)
				{
					t_tree[i].join();
				}
				}catch(InterruptedException e){
				
			}
			sendResponse(MESSAGE_TYPE_ACK, schannel);
			printMessage(MESSAGE_TYPE_ACK, hostId, hostFrom, true);
			tree_complete = true;
			System.out.println("Node "+ hostId + ": Building tree: responded by all its neighbors");
			//System.out.println("");
			System.out.print("Node " + hostId + " has " + TreeNeighborsNo + " tree Neighbors. They are/It is: ");
			for(int i = 0; i< TreeNeighborsNo; i++)
			{
				System.out.print(" " + TreeNeighbors[i]);
			}
			System.out.println("");
			//System.out.println("");

			try
			{
				schannel.close();
			}catch(IOException e)
			{
				
			}
		}
		void Broadcast(SctpChannel schannel)
		{
		
			Thread[] t_bc = new Thread[10];
			printMessage(MESSAGE_TYPE_BROADCAST, hostFrom, hostId, false);
			if(TreeNeighborsNo == 1 && schannel != null)
			{
				sendResponse(MESSAGE_TYPE_BROADCAST_RESPONSE, schannel);
				printMessage(MESSAGE_TYPE_BROADCAST_RESPONSE, hostId, hostFrom, true);
				//System.out.println("");
				try
				{
					schannel.close();
					Project1.bc_count ++;
					Project1.sem.release();
				}catch(IOException e)
				{
					System.out.println("Node "+ hostId + ": BroadCast: channel close error");
				}
				return;
			}
			for(int i = 0; i<TreeNeighborsNo; i++)
			{
				int id = TreeNeighbors[i];
				if(id != hostFrom)
				{
					SctpClient sc = new SctpClient(id, MESSAGE_TYPE_BROADCAST);
					t_bc[i] = new Thread(sc);
					t_bc[i].start();
				}
			}
			try{
					for(int i = 0; i<TreeNeighborsNo; i++)
					{
						int id = TreeNeighbors[i];
						if(id != hostFrom)
							t_bc[i].join();
					}
					System.out.println("Node " + hostId +" BroadCast: responded by all its neighbors");
				}catch(InterruptedException e){
				System.out.println("Boadcast, join, InterruptedException" + e.getMessage());
				
			}
			sendResponse(MESSAGE_TYPE_BROADCAST_RESPONSE, schannel);
			printMessage(MESSAGE_TYPE_BROADCAST_RESPONSE, hostId, hostFrom, true);
			//System.out.println("");
			try
			{
				schannel.close();
				Project1.bc_count ++;
				Project1.sem.release();
			}catch(IOException e)
			{
				System.out.println("BroadCast: channel close error");
			}
			//System.out.println("");
		}
	}
	static void printMessage(int type, int hostfrom, int hostto, boolean bl_send)
	{
		String str = bl_send?"is sent":"is received";
		String strto = new String(" to " + hostto);
		String strfrom = new String(" from " + hostfrom);
		String message = null;
		if(bl_send)
		{
			message = strto;
		}
		else
		{
			message = strfrom;
		}
		switch(type)
		{
		case 0:
			System.out.println("Node "+ hostId + ": Building Tree: Message " + str + message);
			break;
		case 1:
			System.out.println("Node "+ hostId + ": BroadCast: Message " + str + message);
			break;
		case 2:
			System.out.println("Node "+ hostId + ": Building Tree: ACK " + str + message);
			break;
		case 3:
			System.out.println("Node "+ hostId + ": Building Tree: NACK " + str + message);
			break;
		case 4:
			System.out.println("Node "+ hostId + ": BroadCast: Response " + str + message);
			break;
		default:
			break;
		}
	}
	static void sendResponse(int type, SctpChannel schannel)
	{
		ByteBuffer byteBuffer = ByteBuffer.allocate(MESSAGE_SIZE);
		String message = new String(type+"|"+hostId);
		MessageInfo messageInfo = MessageInfo.createOutgoing(null,0);
		
		byteBuffer.put(message.getBytes());
		
		byteBuffer.flip();
		try{
		schannel.send(byteBuffer,messageInfo);
		}catch(IOException e){
			
		}
		return;
	}
	void SRC_buildSpanningTree(SctpChannel schannel)//main thread
	{
		Thread[] t_tree = new Thread[10];
		System.out.println("Node "+ hostId + ": Spanning tree begins to build from source node! The number of neighbors is " + FileNeighborsNo);

		for(int i = 0; i<FileNeighborsNo; i++)
		{
			SctpClient sc = new SctpClient(FileNeighbors[i], MESSAGE_TYPE_BUILD_TREE);
			t_tree[i] = new Thread(sc);
			t_tree[i].start();
		}
		try{
			for(int i = 0; i<FileNeighborsNo; i++)
			{
				t_tree[i].join();
			}
			System.out.println("Node "+ hostId + ": Spanning tree is built!");
			//System.out.println("");
			System.out.print("Node "+ hostId + ": Node " + hostId + " has " + TreeNeighborsNo + " tree Neighbors. They are/It is: ");
			
			for(int i = 0; i< TreeNeighborsNo; i++)
			{
				System.out.print(" " + TreeNeighbors[i]);
			}
			//System.out.println("");
			System.out.println("");
		}catch(InterruptedException e){
			
		}
		tree_complete = true;

	}
	void SRC_Broadcast(SctpChannel schannel)//main thread
	{
		
		Thread[] t_bc = new Thread[10];
		//System.out.println("");
		System.out.println("Node "+ hostId + ": As a source node, Starting to broadCast!");
		
		for(int i = 0; i<TreeNeighborsNo; i++)
		{
			SctpClient sc = new SctpClient(TreeNeighbors[i], MESSAGE_TYPE_BROADCAST);
			t_bc[i] = new Thread(sc);
			t_bc[i].start();
		}
		try{
			for(int i = 0; i<TreeNeighborsNo; i++)
			{
				t_bc[i].join();
			}
			System.out.println("Node "+ hostId + ": BroadCast completes!");
			//System.out.println("");
		}catch(InterruptedException e){
			
		}
		
	}
	public static int getNumber(String s)
	{
		int n = 0;
		
		String[] str = s.split("[^0-9]");
		n = Integer.valueOf(str[0]);
		return n;
	}
	public static String byteToString(ByteBuffer byteBuffer)
	{
		byteBuffer.position(0);
		byteBuffer.limit(MESSAGE_SIZE);
		byte[] bufArr = new byte[byteBuffer.remaining()];
		byteBuffer.get(bufArr);
		return new String(bufArr);
	}
	class SctpClient implements Runnable{
		int conn_Id = 0;
		int mesType = 0;
		
		SctpClient(int connectId, int messageType)
		{
			conn_Id = connectId;
			mesType = messageType;
			

		}
		
		public void run()
		{
			ByteBuffer byteBuffer = ByteBuffer.allocate(MESSAGE_SIZE);
			String message = null;
			
			SocketAddress remoteAddress = new InetSocketAddress(allNodes.get(conn_Id -1).hostName + HOST_SUFFIX
																	,allNodes.get(conn_Id-1).port);
			try{
				SctpChannel sc = SctpChannel.open();
				//sc.bind(new InetSocketAddress(in_port));
				sc.connect(remoteAddress);
				MessageInfo messageInfo = MessageInfo.createOutgoing(null,0);
				byteBuffer.put(new String(mesType + "|" + hostId).getBytes());
				byteBuffer.flip();
				sc.send(byteBuffer,messageInfo);
				
				printMessage(mesType, hostId, conn_Id, true);
			
				ByteBuffer bBuffer = ByteBuffer.allocate(MESSAGE_SIZE);
				sc.receive(bBuffer, null, null);
				
				message = byteToString(bBuffer);
				String[] str_split = message.split("\\|");
				//System.out.println("After split " + str_split[0] + " and " + str_split[1]);
				int type = getNumber(str_split[0]);
				printMessage(type, conn_Id, hostId, false);
				switch(type)
				{
				case 2://ACK
					TreeNeighbors[TreeNeighborsNo++] = conn_Id;
					//System.out.println("Tree neighbor added " + TreeNeighbors[TreeNeighborsNo -1] + " TreeNeighborsNo " + TreeNeighborsNo);
					return;
				case 3://NACK
					return;
				case 4://BC REAPONSE
					return;
				case 0:
				case 1:
				default:
					break;
				}
				sc.shutdown();
				sc.close();
				
			}catch(IOException e){
				System.out.println("Node "+ hostId + ": SctpClient: IOEXCEPTION: " + e);
				
			}
		}
	}
	class SctpServer implements Runnable{
		
		SctpServer()
		{
			
		}
		void executeRequest(SctpChannel sc)
		{
			//Buffer to hold messages in byte format
			//System.out.println("Node "+ hostId + ": Sctp Server receives message");
			ByteBuffer byteBuffer = ByteBuffer.allocate(MESSAGE_SIZE);
			String message;
			try{
			
			MessageInfo messageInfo = sc.receive(byteBuffer,null,null);
			message = byteToString(byteBuffer);
			//System.out.println(message + " " + message.length());
			
			String[] str_splits = message.split("\\|");
			//System.out.println("After split " + str_splits[0] + "and" + str_splits[1]+"test"+ str_splits[1].length());
			int type = getNumber(str_splits[0]);
			int hostFrom = getNumber(str_splits[1]);
			
			if(type == MESSAGE_TYPE_BUILD_TREE)
			{
				if(true == first_build_mes)
				{
					first_build_mes = false;
					TreeNeighbors[TreeNeighborsNo++] = hostFrom; //parent!
					//System.out.println("Tree neighbor added " + TreeNeighbors[TreeNeighborsNo -1] + "TreeNeighborsNo " + TreeNeighborsNo);
					printMessage(type, hostFrom, hostId, false);
					BroadCast_Service bc = new BroadCast_Service(MESSAGE_TYPE_BUILD_TREE, sc, hostFrom);//Integer.valueOf(str_splits[1]));
					Thread t = new Thread(bc);
					t.start();
					/*try
					{
						t.join();
					}catch(InterruptedException e)
					{
						
					}*/
				}
				else
				{
					printMessage(MESSAGE_TYPE_NACK, hostId, hostFrom, true);
					sendResponse(MESSAGE_TYPE_NACK, sc);
				}
				
			}
			else if(type == MESSAGE_TYPE_BROADCAST)
			{
				BroadCast_Service bc = new BroadCast_Service(type, sc, hostFrom);
				Thread t = new Thread(bc);
				t.start();
				
			}
			}catch(IOException e){
				System.out.println("Node "+ hostId + ": SctpServer: IOEXCEPTION: " + e);
				
			}
		}
		public void run()
		{
			try
			{
				SctpServerChannel ssc= SctpServerChannel.open();
				InetSocketAddress serverAddr = new InetSocketAddress(allNodes.get(hostId-1).port);
				ssc.bind(serverAddr);
				//System.out.println("Node "+ hostId + ": SctpServer is running!");
				while(true)
				{
					SctpChannel sc = ssc.accept();
					executeRequest(sc);
					
				}

			}
			catch(IOException ex)
			{
				System.out.println("Node "+ hostId + ": SctpServer: IOException: " + ex.getMessage());
				ex.printStackTrace();
			}
		
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println("CS6378 Project1 by Xiangyu Wang!");
		if(args.length == 0)
		{
			System.out.println("Please input the host ID");
			return;
		}
		
		 Project1 myProject = new Project1();
		 Project1.hostId = Integer.valueOf(args[0]);
		 //Project1.hostId = 2;
		// System.out.println("This is Host " + Project1.hostId);
		 myProject.readConfigFile(Project1.hostId);
		 myProject.runServer();
	
		 if(Project1.bc_count == Project1.bc_order - 1)
		 {
			 //System.out.println("main: begin!");
			 if(Project1.tree_complete != true)
			 {
				 try
				 {
					 Thread.sleep(1000);
				 }catch(InterruptedException e)
				 {
					 
				 }
				 Project1.first_build_mes = false;
				 myProject.SRC_buildSpanningTree(null);
			 }
			
			
			 myProject.SRC_Broadcast(null);
			 Project1.bc_count ++;
		 }
		 else
		 {
			 while(true)
			 {
				 try
				 {
					 Project1.sem.acquire();
					 //System.out.println("count = " + Project1.bc_count + "order = " + Project1.bc_order);
					 if(Project1.bc_count == Project1.bc_order - 1)
					 {
						 try
						 {
							 Thread.sleep(200);
						 }catch(InterruptedException e)
						 {
							 
						 }
						 myProject.SRC_Broadcast(null);
						 Project1.bc_count ++;
					 }
				 }catch(InterruptedException e){
					 
				 }
			 }
		 }

	}

}
